# Vehicle routing problem with time windows

The vehicle routing problem with time windows is concerned with the optimal routing of a ﬂeet of vehicles between a depot and a number of customers that must be visited 
within a speciﬁed time interval, called a time window. The purpose of this thesis is to develop new and efﬁcient solution techniques for solving the vehicle routing 
problem with time windows (VRPTW).

## How to run in docker

1. Open vrptw folder
2. Run this in terminal: *docker-compose up --build*
3. Use <http://localhost:3000>

## How to run on local machine

1. Open vrptw/server folder
2. Run this in terminal: *npm install && mongod && npm run dev*
3. Open vrptw/server folder
4. Run this in terminal: *npm install && npm start*
5. Use <http://localhost:3000>

## heroku link
https://check-time-and-go-app.herokuapp.com/