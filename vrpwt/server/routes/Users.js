const express = require("express");
const users = express.Router();
const cors = require("cors");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../models/User");
const IncomingForm = require("formidable").IncomingForm;
const fs = require("fs");

let { PythonShell } = require("python-shell");

users.use(cors());

process.env.SECRET_KEY = "secret";

users.post("/register", (req, res) => {
  const today = new Date();
  const userData = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password,
    created: today
  };

  User.findOne({
    email: req.body.email
  })
    .then(user => {
      if (!user) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          userData.password = hash;
          User.create(userData)
            .then(user => {
              res.json({ registrationSuccesful: true });
            })
            .catch(err => {
              res.send("error: " + err);
            });
        });
      } else {
        return res.json({ registrationSuccesful: false });
      }
    })
    .catch(err => {
      res.send("error: " + err);
    });
});

users.post("/login", (req, res) => {
  User.findOne({
    email: req.body.email
  })
    .then(user => {
      if (user) {
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const payload = {
            _id: user._id,
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email
          };
          let token = jwt.sign(payload, process.env.SECRET_KEY, {
            expiresIn: "1h"
          });
          return res.json({
            token: token
          });
        } else {
          return res.json({
            error: true
          });
        }
      } else {
        return res.json({
          error: true
        });
      }
    })
    .catch(err => {
      res.send("error: " + err);
    });
});

users.post("/problemsolver", (req, res) => {
  var vehicle_capacity = req.body.vehicle_capacity;
  var dataArray = JSON.stringify(req.body.data);
  var options = {
    args: [dataArray, vehicle_capacity]
  };
  PythonShell.run(__dirname + "/../VRPTW_solver/main.py", options, function(
    err,
    data
  ) {
    if (err) {
      if (
        err.message.match(
          /Wrong time parameters for Xcoord: [0-9.]+ and Ycoord: [0-9.]+/g
        )
      ) {
        res.send(err.message);
      } else throw err;
    } else {
      new Promise((resolve, reject) => {
        resolve(JSON.parse(data));
      })
        .then(value => {
          res.send(value);
        })
        .catch(error => {
          console.log(error);
        });
    }
  });
});

users.post("/upload", (req, res) => {
  var form = new IncomingForm();
  form.parse(req, function(err, fields, files) {
    if (err) throw err;
    var oldpath = files.file.path;
    var newpath = __dirname + "/../uploads/" + files.file.name;
    fs.rename(oldpath, newpath, function(err) {
      if (err) throw err;
      res.json({ data: "success" });
    });
  });
});

module.exports = users;
