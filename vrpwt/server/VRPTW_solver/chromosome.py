from nodes import Customer as Node
from nodes import CustomerDistanceTable as Edge
import ga_params
import utils
import sys


distance_table = Edge
customers = [Node]


class Chromosome:
    route = []                  # type: list
    value = None                # type: float

    vehicles_count = None       # type: int
    vehicles_routes = None      # type: list
    route_rounds = None         # type: int
    total_travel_dist = None    # type: float
    total_elapsed_time = None   # type: float
    total_wait_time = None
    current_load = None         # type: float
    elapsed_time = None         # type: float
    max_elapsed_time = None     # type: float
    wait_time = None            # type: float

    def __init__(self, route: iter):
        self.vehicles_count = 1
        self.vehicles_routes = [[0]]
        self.route_rounds = 0
        self.total_travel_dist = 0
        self.total_elapsed_time = 0
        self.total_wait_time = 0
        self.current_load = 0
        self.elapsed_time = 0
        self.max_elapsed_time = 0
        self.wait_time = 0

        self.route = list(route)
        self.value = self.get_cost_score()

    @staticmethod
    def get_travel_time(distance: float) -> float:
        return distance / ga_params.vehicle_speed_avg

    @staticmethod
    def get_travel_cost(distance: float) -> float:
        return distance * ga_params.vehicle_cost_per_dist

    @staticmethod
    def get_distance(source: float, dest: float) -> float:
        return distance_table.get_distance(source, dest)

    @staticmethod
    def get_node(index: int) -> Node:
        return customers[index]

    def isValidTimeParams(self, dest_customer: Node) -> bool:
        # ban!
        if dest_customer.due_time <= dest_customer.ready_time:
            sys.exit("Wrong time parameters for Xcoord: " +
                     str(dest_customer.x) + " and Ycoord: " + str(dest_customer.y))

    def check_time_and_go(self, source: int, dest: int) -> bool:
        dest_customer = self.get_node(dest)  # type: Node

        self.isValidTimeParams(dest_customer)

        distance = self.get_distance(source, dest)
        elapsed_new = self.get_travel_time(distance) + self.elapsed_time
        if elapsed_new <= dest_customer.due_time:
            return_time = self.get_travel_time(self.get_distance(dest, 0))
            deport_due_time = self.get_node(0).due_time
            if elapsed_new + dest_customer.service_time + return_time <= deport_due_time:
                self.move_vehicle(source, dest)
                return True
            else:
                return False
        else:
            return False

    def check_capacity(self, dest: float) -> bool:
        dest_customer = self.get_node(dest)  # type: Node
        return self.current_load + dest_customer.demand <= ga_params.vehicle_capacity

    @staticmethod
    def get_vehicle_count_with_max_elapsed_time(vehicles_count: int, deport_working_hours: int) -> float:
        return ga_params.vehicles_count_over_deport_hours_preference * vehicles_count + deport_working_hours

    def move_vehicle(self, source: float, dest: float):
        distance = self.get_distance(source, dest)
        self.total_travel_dist += distance
        self.wait_time = max(0, self.get_node(
            dest).ready_time - self.elapsed_time)
        self.total_wait_time += self.wait_time
        self.elapsed_time += self.get_travel_time(distance) + self.wait_time
        self.total_elapsed_time += self.elapsed_time
        self.max_elapsed_time = max(self.elapsed_time, self.max_elapsed_time)
        self.vehicles_routes[-1].append(dest)

        if dest == 0:
            self.route_rounds += 1
            self.current_load = 0
        else:
            dest_customer = self.get_node(dest)  # type: Node
            self.current_load += dest_customer.demand
            if self.current_load < 0:
                self.current_load = 0

    def add_vehicle(self):
        self.vehicles_count += 1
        self.vehicles_routes.append([0])
        self.elapsed_time = 0
        self.wait_time = 0
        self.current_load = 0

    def get_cost_score(self) -> float:
        for source, dest in utils.pairwise([0] + self.route + [0]):
            if self.check_capacity(dest):
                if not self.check_time_and_go(source, dest):
                    self.move_vehicle(source, 0)
                    self.add_vehicle()
                    self.move_vehicle(0, dest)
            else:
                self.move_vehicle(source, 0)
                if not self.check_time_and_go(0, dest):
                    self.add_vehicle()
                    self.move_vehicle(0, dest)

        total_travel_cost = self.get_travel_cost(self.total_travel_dist)
        return_value = self.get_vehicle_count_with_max_elapsed_time(
            vehicles_count=self.vehicles_count,
            deport_working_hours=self.max_elapsed_time)
        return return_value + total_travel_cost

    def __iter__(self):
        for item in self.route:
            yield item

    def __len__(self):
        return len(self.route)

    def __getitem__(self, index):
        return self.route[index]

    def __setitem__(self, index, value):
        self.route[index] = value

    def __lt__(self, other):
        return self.value < other.value

    def __eq__(self, other):
        return self.value == other.value

    def __hash__(self):
        return hash(self.value)

    def __add__(self, other):
        return self.value + other

    def __sub__(self, other):
        return self.value - other

    def __float__(self):
        return float(self.value)

    def __str__(self):
        return str(self.route) + ", value= " + str(self.value) + ", vehicles_count= " + str(self.vehicles_count) \
            + ", total deport visits=" + str(self.route_rounds) \
            + ", deport working hours=" + \
            str(self.max_elapsed_time) + \
            ", routes= " + str(self.vehicles_routes)

    def __repr__(self):
        return self.__str__()
