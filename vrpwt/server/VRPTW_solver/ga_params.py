ITERATION_SIZE = 100

vehicle_cost_per_dist = 1.0
vehicle_speed_avg = 1.0
vehicle_capacity = 200
vehicles_count_over_deport_hours_preference = 1000

run_file = {
    'name': 'C101_200',
    'header_map': {
        'CUST_NO': 'id',
        'XCOORD': 'x_coordinates',
        'YCOORD': 'y_coordinates',
        'DEMAND': 'demand',
        'READY_TIME': 'ready_time',
        'DUE_DATE': 'due_time',
        'SERVICE_TIME': 'service_time',
    }
}

population = {
    'population_size': 50,
    'mutation_rate': 0.2,
    'generation_size' : 50,     
}
