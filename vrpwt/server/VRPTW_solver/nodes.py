from datetime import datetime
import ga_params
import math



class Customer:
    x = None             # type: float
    y = None             # type: float
    demand = None        # type: float
    ready_time = None    # type: float
    due_time = None      # type: float
    service_time = None  # type: float

    def __init__(self, x_coordinates: float, y_coordinates: float, demand: float, ready_time: int, due_time: int,
                 service_time: int, **kwargs):
        self.x = float(x_coordinates)
        self.y = float(y_coordinates)
        self.demand = float(demand)
        self.ready_time = int(float(ready_time))
        self.due_time = int(float(due_time))
        self.service_time = int(float(service_time))

    def __str__(self):
        return '\ncoordinated: ' + str(self.x) + ' - ' + str(self.y)\
               + ' | demand: ' + str(self.demand) + ' | ready_time: ' + str(self.ready_time)\
               + ' | due_time: ' + str(self.due_time) + ' | service_time: ' + str(self.service_time)

    def __repr__(self):
        return self.__str__
   

class CustomerDistanceTable:
    distance_table = None   # type: list

    def get_distance(self, source: float, dest: float) -> float:
        return self.distance_table[source][dest]
    
    @staticmethod
    def get_distance_customers_pair(c1: Customer, c2: Customer) -> float:
        return math.hypot(c2.x - c1.x, c2.y - c1.y)

    def __init__(self, customer_list):
        # cost_table_timer = datetime.now()
        self.distance_table = []
        for needle_index, customer_needle in enumerate(customer_list):

            self.distance_table.append([])
            for in_stack_index, customer_in_stack in enumerate(customer_list):

                if needle_index == in_stack_index:
                    cost = 0
                elif needle_index > in_stack_index:
                    cost = self.distance_table[in_stack_index][needle_index]
                else:
                    cost = self.get_distance_customers_pair(customer_needle, customer_in_stack)
    
                self.distance_table[needle_index].append(cost)
    
    def __str__(self):
        table_str = "\n--- Travel Cost between Customers\n"
        for row in self.distance_table:
            table_str += str(row)+'\n\n'
        table_str += "\n--- END | Travel Cost Between Customers\n"
        return table_str

    def __repr__(self):
        return self.__str__()


