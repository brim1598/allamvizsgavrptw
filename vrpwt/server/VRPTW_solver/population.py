from random import shuffle
from datetime import datetime
from chromosome import Chromosome
import random
import ga_params


List_Chromosome = [Chromosome]


class Population():
    generation = []                     # type: List_Chromosome
    iteration_index = 0                 # type: int
    population_size = None              # type: int
    chromosome_width = None             # type: int
    mutation_rate = None                # type: float
    generation_size = None              # type: int

    def __init__(self, mutation_rate: float, population_size: int, generation_size: int, chromosome_width: int):
        self.population_size = population_size
        self.chromosome_width = chromosome_width
        self.mutation_rate = mutation_rate
        self.generation_size = generation_size
        self.generation = sorted(
            self.initial_generation(), key=lambda x: x.value)
        # for i , val in enumerate(self.generation):
        #     print (i, val)

    def initial_generation(self, init_size: int = None) -> List_Chromosome:
        if not init_size:
            init_size = self.population_size
        generation_holder = []
        random_indices = list(range(1, self.chromosome_width))
        for _ in range(init_size):
            shuffle(random_indices)
            init_chromosome = Chromosome(random_indices)
            generation_holder.append(init_chromosome)
        return generation_holder

    def binary_tournament_selection(self, generation: List_Chromosome) -> list:
        k = 2
        selected = []
        population_copy = list(generation)
        limit = len(generation)
        for _ in range(limit):
            shuffle(population_copy)
            tournament = population_copy[:k]
            selected_in_tournament = min(tournament)
            population_copy.remove(selected_in_tournament)
            selected.append(selected_in_tournament)
        shuffle(selected)
        selected = selected[:k]
        return selected

    # def crossover(self, twoparents: list) -> (list, list):
    #     # ordered crossover
    #     ind1, ind2 = twoparents[0], twoparents[1]
    #     size = min(len(ind1), len(ind2))
    #     a, b = random.sample(range(size), 2)
    #     if a > b:
    #         a, b = b, a
    #     holes1, holes2 = [True]*size, [True]*size
    #     for i in range(size):
    #         if i < a or i > b:
    #             holes1[ind2[i]-1] = False
    #             holes2[ind1[i]-1] = False
    #     # We must keep the original values somewhere before scrambling everything
    #     temp1, temp2 = ind1, ind2
    #     k1, k2 = b + 1, b + 1
    #     for i in range(size):
    #         if not holes1[temp1[(i + b + 1) % size]-1]:
    #             ind1[k1 % size] = temp1[(i + b + 1) % size]
    #             k1 += 1
    #         if not holes2[temp2[(i + b + 1) % size]-1]:
    #             ind2[k2 % size] = temp2[(i + b + 1) % size]
    #             k2 += 1
    #     # Swap the content between a and b (included)
    #     for i in range(a, b + 1):
    #         ind1[i], ind2[i] = ind2[i], ind1[i]

    #     return ind1, ind2

    def crossover(self, twoparents: list) -> (list, list):
        # ordered crossover
        ind1, ind2 = list(twoparents[0]), list(twoparents[1])
        size = min(len(ind1), len(ind2))
        a, b = random.sample(range(size), 2)
        if a > b:
            a, b = b, a

        for i in range(size):
            if i < a or i > b:
                ind1[i] = -1
                ind2[i] = -1

        jj1 = jj2 = b+1
        # We must keep the original values somewhere before scrambling everything
        for k in range(b+1, size):
            if twoparents[1][k] not in ind1:
                ind1[jj1] = twoparents[1][k]
                jj1 = jj1+1
            if twoparents[0][k] not in ind2:
                ind2[jj2] = twoparents[0][k]
                jj2 = jj2+1

        for k in range(0, b+1):
            if jj1 == size:
                jj1 = 0
            if jj2 == size:
                jj2 = 0
            if twoparents[1][k] not in ind1:
                ind1[jj1] = twoparents[1][k]
                jj1 = jj1+1
            if twoparents[0][k] not in ind2:
                ind2[jj2] = twoparents[0][k]
                jj2 = jj2+1
        return Chromosome(ind1), Chromosome(ind2)


    def mutation(self, chromosome: list) -> list:
        slice_point_1 = random.randint(0, len(chromosome) - 3)
        slice_point_2 = random.randint(slice_point_1 + 2, len(chromosome) - 1)
        return Chromosome(chromosome[:slice_point_1] + list(reversed(chromosome[slice_point_1:slice_point_2])) + chromosome[slice_point_2:])

    def remove_duplicates_from_list(self, generation: List_Chromosome) -> List_Chromosome:
        return list(dict.fromkeys(generation))

    def evolve(self, generation: List_Chromosome, iteration_index: int) -> Chromosome:
        while iteration_index < ga_params.ITERATION_SIZE * self.chromosome_width:
            if (len(generation) > 1):
                twoparents = self.binary_tournament_selection(generation)
                child1, child2 = self.crossover(twoparents=twoparents)
                offsprings_holders = [child1, child2]
                randindex = random.sample(range(2), 1)
                new_chromosome = offsprings_holders[randindex[0]]
                if (random.random() < self.mutation_rate):
                    new_chromosome = self.mutation(
                        offsprings_holders[randindex[0]])
            else:
                new_chromosome = generation[0]
                if (random.random() < self.mutation_rate):
                    new_chromosome = self.mutation(
                        new_chromosome)

            for k_index in range(len(generation)):
                if new_chromosome.value <= generation[k_index].value:
                    self.iteration_index = self.iteration_index + 1
                    iteration_index = iteration_index + 1
                    generation.append(new_chromosome)
                    index_max = generation.index(max(generation))
                    generation.remove(generation[index_max])

            generation = self.remove_duplicates_from_list(
                generation=generation)

        return min(generation)

    def make_generation(self):
        generation_list = []
        for _ in range(self.generation_size):
            generation_list.append(
                self.evolve(generation=self.generation, iteration_index=0))
        generation_list = self.remove_duplicates_from_list(
                generation=generation_list)
        self.generation = sorted(
            generation_list, key=lambda x: x.value)

    def get_bestChromosome(self):
        return self.generation[0]

    def __str__(self):
        pop_str = "Generation:" + str(self.iteration_index) + '\n'
        for i, chromosome in enumerate(self.generation):
            pop_str += str(i + 1) + ': ' + str(chromosome) + '\n'
        return pop_str

    def __repr__(self):
        return self.__str__()
