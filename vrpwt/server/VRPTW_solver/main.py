from nodes import Customer, CustomerDistanceTable
from csv_reader import csv_read
import ga_params
import chromosome
import population
import sys
import json
import time

customers = None

if (len(sys.argv) < 2):
    run_file_name = input("Enter run file name [default: C101_200]: ")
    if not run_file_name:
        run_file_name = ga_params.run_file['name']
    print('Running: "' + run_file_name + '"')
    customers_input_read = csv_read(
        run_file_name, header_map=ga_params.run_file['header_map'])
    customers = [Customer(**customers_input_read[0])]
    customers += [Customer(**customer_dict)
                  for customer_dict in customers_input_read]
else:
    customers_input_read = json.loads(sys.argv[1])
    customers = [Customer(**customer_dict)
                 for customer_dict in customers_input_read]
    ga_params.vehicle_capacity = int(sys.argv[2])

# ga_params.vehicle_capacity = 50

# for value in customers_input_read:
#     for value2 in value:

# print(type(customers_input_read),customers_input_read)

# for i, c in enumerate(customers):
#     print(i,c)

distance_table = CustomerDistanceTable(customers)  # matrix
# print(distance_table)

chromosome.customers = customers
chromosome.distance_table = distance_table

# chromosome1 = chromosome.Chromosome([3, 9, 7, 6, 1, 5, 2, 10, 8, 4])
# print(chromosome1.value)
# for i in chromosome1:
#     print(i)

#start_time = time.time()
ga_population = population.Population(
    **ga_params.population, chromosome_width=len(chromosome.customers))

# original solution
#print(ga_population.get_bestChromosome())
#print("===============================================================")


# best solution
ga_population.make_generation()
best_chromosome = ga_population.get_bestChromosome()
print(best_chromosome.vehicles_routes)
# print(best_chromosome)

# print("--- %s seconds ---" % (time.time() - start_time))
sys.stdout.flush()
