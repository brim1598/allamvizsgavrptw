import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import MyNavbar from "./components/Navbar";
import Landing from "./components/Landing";
import Login from "./components/Login";
import Register from "./components/Register";
import Profile from "./components/Profile";
import ErrorNotFound from "./components/ErrorNotFound";
import ProblemSolver from "./components/ProblemSolver";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <MyNavbar />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Landing} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/profile" component={Profile} />
              <Route exect path="/vrptw" component={ProblemSolver} />
              <Route component={ErrorNotFound} />
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
