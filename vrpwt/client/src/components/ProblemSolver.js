import React, { Component } from "react";
import jwt_decode from "jwt-decode";
import "leaflet/dist/leaflet.css";
import { Map, TileLayer } from "react-leaflet";
import L from "leaflet";
import LocateControl from "./LocateControl";
import { withLeaflet } from "react-leaflet";
import { ReactLeafletSearch } from "react-leaflet-search";
import Table from "./ReactTable";
import "leaflet-control-geocoder/dist/Control.Geocoder.js";
import Control from "react-leaflet-control";
import Button from "react-bootstrap/Button";
import { withStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import { problemSolver } from "./UserFunctions";
import "leaflet-routing-machine/src";
import "leaflet-routing-machine/dist/leaflet-routing-machine.css";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import Cleave from "cleave.js/react";
import { Alert } from "reactstrap";
import ReactDOM from "react-dom";

delete L.Icon.Default.prototype._getIconUrl;

// eslint-disable-next-line
Date.prototype.format = function(fmt) {
  var date = this;

  return fmt.replace(/\{([^}:]+)(?::(\d+))?\}/g, function(s, comp, pad) {
    var fn = date["get" + comp];

    if (fn) {
      var v = (fn.call(date) + (/Month$/.test(comp) ? 1 : 0)).toString();

      return pad && (pad = pad - v.length)
        ? new Array(pad + 1).join("0") + v
        : v;
    } else {
      return s;
    }
  });
};

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});

const style = theme => ({
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

class ProbleSolver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      vehicle_capacity: 200,
      routeControl: null,
      loaded: false,
      receivedFromServer: [],
      selectedRoute: ""
    };
    this.deleteLastMarker = this.deleteLastMarker.bind(this);
    this.sendDatasToServer = this.sendDatasToServer.bind(this);
    this.initRoutingControl = this.initRoutingControl.bind(this);
    this.createPath = this.createPath.bind(this);
    this.createPathWithParam = this.createPathWithParam.bind(this);
    this.setVehicleCapacity = this.setVehicleCapacity.bind(this);
    this.selectRouteFromCards = this.selectRouteFromCards.bind(this);
  }

  componentDidMount() {
    if (localStorage.usertoken !== undefined) {
      const token = localStorage.usertoken;
      const decoded = jwt_decode(token);
      this.setState({
        first_name: decoded.first_name,
        last_name: decoded.last_name,
        email: decoded.email
      });
    } else {
      this.props.history.push(`/login`);
    }
  }

  setVehicleCapacity = value => {
    this.setState({
      vehicle_capacity: value > 0 ? value : 1
    });
  };

  selectRouteFromCards = e => {
    var str = e.target.previousSibling.textContent.split(" ");
    this.setState({
      selectedRoute: str[0]
    });

    const value = e.target.parentNode.childNodes[0].value.split(",");
    const array = [];
    value.forEach(element => {
      if (element < this.state.markers.length) {
        array.push(this.state.markers[parseInt(element, 10)]);
      }
    });
    this.createPathWithParam(array);
  };

  deleteLastMarker = () => {
    var elem = document.getElementsByClassName(
      "leaflet-pane leaflet-marker-pane"
    )[0];
    if (
      elem.getElementsByTagName("img").length &&
      this.state.markers.length > 0
    ) {
      var trash = this.state.markers[this.state.markers.length - 1].markerIcon
        ._icon;
      trash.parentNode.removeChild(trash);
      trash = this.state.markers[this.state.markers.length - 1].markerIcon
        ._shadow;
      trash.parentNode.removeChild(trash);
      const { markers } = this.state;
      markers.pop();
      this.setState({ markers });
    }
    this.createPath();
  };

  createPath = () => {
    var waypointList = this.state.markers.map((value, index, array) => {
      return [value["lat"], value["lng"]];
    });
    this.state.routeControl.setWaypoints(waypointList);
  };

  createPathWithParam = array => {
    var waypointList = array.map((value, index, arr) => {
      return [value["lat"], value["lng"]];
    });
    this.state.routeControl.setWaypoints(waypointList);
  };

  initRoutingControl = map => {
    // initializate routeControl
    var _routeControl = L.Routing.control({
      collapsible: true,
      waypoints: [],
      show: false,
      addWaypoints: false,
      draggableWaypoints: false,
      routeWhileDragging: false,
      fitSelectedRoutes: false,
      defaultErrorHandler: false,
      showAlternatives: false,
      altLineOptions: { styles: [{ opacity: 1 }] },
      createMarker: () => {
        return false;
      }
    }).addTo(map);
    this.setState({
      routeControl: _routeControl
    });
  };

  addMarker = e => {
    const map = this.leafletMap.leafletElement;

    if (this.state.routeControl === null) {
      this.initRoutingControl(map);
    }

    const geocoder = L.Control.Geocoder.nominatim();
    let marker;

    new Promise((resolve, reject) =>
      geocoder.reverse(
        e.latlng,
        map.options.crs.scale(map.getZoom()),
        results => {
          var r = results[0];
          if (r) {
            if (marker) {
              marker
                .setLatLng(r.center)
                .setPopupContent(r.html || r.name)
                .openPopup();
            } else {
              marker = L.marker(r.center).bindPopup(r.name);
              const result = this.state.markers.filter(
                obj => obj.lat === r.center.lat && obj.lng === r.center.lng
              );
              if (!result.length) {
                marker.addTo(map).openPopup();
                r.center["id"] = this.state.markers.length;
                r.center["markerIcon"] = marker;
                r.center["name"] = r.name;
                r.center["demand"] = 0;
                r.center["ready_date"] = new Date().format(
                  "{FullYear}/{Month:2}/{Date:2}"
                );
                r.center["ready_time"] = "00:00";
                r.center["due_date"] = new Date().format(
                  "{FullYear}/{Month:2}/{Date:2}"
                );
                r.center["due_time"] = "23:59";
                resolve(r.center);
              }
            }
          }
        }
      )
    ).then(value => {
      const { markers } = this.state;
      markers.push(value);
      this.setState({ value });
      this.createPath();
    });
  };

  sendDatasToServer = e => {
    this.setState({ receivedFromServer: [] });
    this.setState({ loaded: true });
    this.setState({
      selectedRoute: ""
    });

    e.preventDefault();

    const data = this.state.markers.map((value, index, array) => {
      var readyTimeInInteger = new Date(
        value["ready_date"] + " " + value["ready_time"].replace(/-/g, "/")
      ).getTime();
      var dueTimeInInteger = new Date(
        value["due_date"] + " " + value["due_time"].replace(/-/g, "/")
      ).getTime();
      var val = {
        id: value["id"],
        x_coordinates: value["lat"],
        y_coordinates: value["lng"],
        demand: value["demand"] || 0,
        ready_time: readyTimeInInteger,
        due_time: dueTimeInInteger,
        service_time: 90.0
      };
      return val;
    });

    problemSolver(data, this.state.vehicle_capacity).then(res => {
      let element = "";
      if (res) {
        if (typeof res === "string" || res instanceof String) {
          this.setState({ loaded: false });
          element = <Alert color="danger">{res}</Alert>;
          ReactDOM.render(
            element,
            document.getElementsByName("responseDiv")[0]
          );
        } else {
          ReactDOM.render(
            element,
            document.getElementsByName("responseDiv")[0]
          );
          this.setState({ loaded: false });
          this.setState({ receivedFromServer: res });
          var flattedArray = res.flat();
          flattedArray = flattedArray.filter((value, index, arr) => {
            return value !== 0;
          });
          flattedArray.unshift(0);
          const array = [];
          flattedArray.forEach(element => {
            array.push(this.state.markers[element]);
          });
          array.push(this.state.markers[0]);
          this.createPathWithParam(array);
        }
      } else {
        console.log("Error in getting data!");
      }
    });
  };

  render() {
    const { classes } = this.props;

    const locateOptions = {
      position: "topleft",
      strings: {
        title: "Show me where I am!"
      },
      onActivate: () => {} // callback before engine starts retrieving locations
    };

    const GeoSearch = withLeaflet(ReactLeafletSearch);

    let solveButton;
    let theBestWay;
    let routeOnMap;

    if (this.state.selectedRoute !== "") {
      routeOnMap = (
        <div>
          <label
            style={{
              fontSize: "120%"
            }}
          >
            The plotted line on the map:{" "}
            <b> route {this.state.selectedRoute}</b>
          </label>
        </div>
      );
    }

    if (this.state.markers.length > 3) {
      let spinner;
      if (this.state.loaded) {
        spinner = (
          <Spinner
            as="span"
            animation="grow"
            size="lg"
            role="status"
            aria-hidden="true"
          />
        );
      }

      solveButton = (
        <Button
          variant="primary"
          size="lg"
          onClick={this.sendDatasToServer}
          block
        >
          {spinner}
          Solve
          <Icon className={classes.rightIcon}>send</Icon>
        </Button>
      );
    }

    if (this.state.receivedFromServer.length) {
      const ways = this.state.receivedFromServer;
      const cards = [];
      for (let i = 0; i < ways.length; i++) {
        let route = "";
        for (let ii = 0; ii < ways[i].length; ii++) {
          if (ii === ways[i].length - 1) {
            route += ways[i][ii];
            break;
          }
          route += ways[i][ii] + " --> ";
        }

        cards.push(
          <div key={i}>
            <input type="hidden" name="route" value={ways[i]} />
            <Card.Title>{i + 1}. route:</Card.Title>
            <Card.Text onClick={e => this.selectRouteFromCards(e)}>
              {route}
            </Card.Text>
            <hr />
          </div>
        );
      }

      theBestWay = (
        <Card>
          <Card.Header>The best way</Card.Header>
          <Card.Body>{cards}</Card.Body>
        </Card>
      );
    }

    return (
      <div>
        <Map
          center={[46.7693367, 23.5900604]}
          onClick={this.addMarker}
          // zoom={10}
          style={{ height: "400px", top: "50px" }}
          ref={m => {
            this.leafletMap = m;
          }}
        >
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
          <GeoSearch position="topleft" />
          <LocateControl options={locateOptions} startDirectly />
          <Control position="topright">
            <button
              style={{ backgroundColor: "white", textDecorationColor: "black" }}
              onClick={() => this.deleteLastMarker()}
            >
              Undo
            </button>
          </Control>
        </Map>
        <div>
          <br />
          <br />
          <br />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <div>
            <label
              style={{
                fontSize: "120%"
              }}
            >
              Vehicle capacity:
            </label>
            {"  "}
            <Cleave
              options={{
                numeral: true,
                numeralThousandsGroupStyle: "none",
                numeralPositiveOnly: true
              }}
              onBlur={e => this.setVehicleCapacity(e.target.value)}
              value={this.state.vehicle_capacity}
            />
          </div>
          {routeOnMap}
        </div>
        <div>
          <br />
        </div>
        <Table
          problemSolverMarkers={this.state} // The data prop should be immutable and only change when you want to update the table
          createPath={this.createPath}
          resolveData={problemSolverMarkers =>
            problemSolverMarkers.map(row => row)
          }
        />
        <div>
          <br />
        </div>
        {solveButton}
        <div>
          <br />
        </div>
        {theBestWay}
        <div>
          <br />
        </div>
        <div name="responseDiv" />
      </div>
    );
  }
}

export default withStyles(style)(ProbleSolver);
