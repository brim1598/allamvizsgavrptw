import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import Cleave from "cleave.js/react";

// function copyArray(src) {
//   return Object.assign([], src);
// }
class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: this.props.problemSolverMarkers.markers
    };
    this.dataChange = this.dataChange.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
  }

  dataChange(event, dataType, value) {
    var index =
      event.target.parentNode.parentNode.parentNode.childNodes[0].textContent;
    const temp = this.state.tableData;
    temp[index][dataType] = value;
    this.setState({ tableData: temp });
  }

  deleteRow(event) {
    var index =
      event.target.parentNode.parentNode.parentNode.childNodes[0].textContent;
    var temp = this.state.tableData;

    // delete marker from map
    var elem = document.getElementsByClassName(
      "leaflet-pane leaflet-marker-pane"
    )[0];
    if (elem.getElementsByTagName("img").length && temp.length > 0) {
      var trash = temp[index].markerIcon._icon;
      trash.parentNode.removeChild(trash);
      trash = temp[index].markerIcon._shadow;
      trash.parentNode.removeChild(trash);
      temp.splice(index, 1);
      temp.map((value, index, array) => {
        value["id"] = array.indexOf(value);
        return value;
      });
      this.setState({ tableData: temp });
    }
    this.props.createPath();
  }

  render() {
    const inputStyle = {
      width: "50%",
      fontSize: "smaller"
    };

    let columns = [
      {
        Header: "ID",
        maxWidth: 40,
        accessor: "id"
      },
      {
        Header: "XCOORD",
        maxWidth: 100,
        accessor: "lat"
      },
      {
        Header: "YCOORD",
        maxWidth: 100,
        accessor: "lng"
      },
      {
        Header: "Location Name",
        maxWidth: 350,
        accessor: "name"
      },
      {
        Header: "DEMAND",
        id: "row1",
        maxWidth: 90,
        Cell: row => {
          return (
            <div>
              <Cleave
                style={inputStyle}
                onBlur={e => this.dataChange(e, "demand", e.target.value)}
                // placeholder="YYYY-mm-dd"
                value={this.state.tableData[row.index]["demand"]}
                options={{
                  numeral: true,
                  numeralThousandsGroupStyle: "none"
                }}
              />
            </div>
          );
        }
      },
      {
        Header: "READY TIME",
        id: "row2",
        maxWidth: 200,
        Cell: row => {
          return (
            <div>
              <Cleave
                style={inputStyle}
                onBlur={e => this.dataChange(e, "ready_date", e.target.value)}
                // placeholder="YYYY-mm-dd"
                value={this.state.tableData[row.index]["ready_date"]}
                options={{ date: true, datePattern: ["Y", "m", "d"] }}
              />
              <Cleave
                style={inputStyle}
                onBlur={e => this.dataChange(e, "ready_time", e.target.value)}
                value={this.state.tableData[row.index]["ready_time"]}
                // placeholder="hh:mm"
                options={{ time: true, timePattern: ["h", "m"] }}
              />
            </div>
          );
        }
      },
      {
        Header: "DUE TIME",
        id: "row3",
        maxWidth: 200,
        Cell: row => {
          return (
            <div>
              <Cleave
                style={inputStyle}
                onBlur={e => this.dataChange(e, "due_date", e.target.value)}
                // placeholder="YYYY-mm-dd"
                value={this.state.tableData[row.index]["due_date"]}
                options={{ date: true, datePattern: ["Y", "m", "d"] }}
              />
              <Cleave
                style={inputStyle}
                onBlur={e => this.dataChange(e, "due_time", e.target.value)}
                value={this.state.tableData[row.index]["due_time"]}
                // placeholder="hh:mm"
                options={{ time: true, timePattern: ["h", "m"] }}
              />
            </div>
          );
        }
      },
      {
        Header: "Delete",
        maxWidth: 40,
        Cell: row => (
          <div>
            <button onClick={event => this.deleteRow(event)}>X</button>
          </div>
        )
      }
    ];

    return (
      <div>
        <ReactTable
          key={this.state.tableData.length}
          defaultPageSize={10}
          data={this.state.tableData}
          columns={columns}
        />
      </div>
    );
  }
}

export default Table;
