import React, { Component } from "react";
import jwt_decode from "jwt-decode";

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      first_name: "",
      last_name: "",
      email: ""
    };
  }

  componentDidMount() {
    if (localStorage.usertoken !== undefined) {
      const token = localStorage.usertoken;
      const decoded = jwt_decode(token);
      this.setState({
        first_name: decoded.first_name,
        last_name: decoded.last_name,
        email: decoded.email
      });
    } else {
      this.props.history.push(`/login`);
    }
  }

  render() {
    const fontStyle = {
      fontSize: "120%",
      fontWeight: "bold"
    };

    const fontStyleUser = {
      fontSize: "110%",
      textAlign: "center"
    };

    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">PROFILE</h1>
          </div>
          <br />
          <table className="table col-md-6 mx-auto">
            <tbody>
              <tr>
                <td style={fontStyle}>First Name:</td>
                <td style={fontStyleUser}>{this.state.first_name}</td>
              </tr>
              <tr>
                <td style={fontStyle}>Last Name:</td>
                <td style={fontStyleUser}>{this.state.last_name}</td>
              </tr>
              <tr>
                <td style={fontStyle}>Email:</td>
                <td style={fontStyleUser}>{this.state.email}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Profile;
