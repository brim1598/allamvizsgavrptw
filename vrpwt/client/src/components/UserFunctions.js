import axios from "axios";

export const register = newUser => {
  return axios
    .post("users/register", {
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      email: newUser.email,
      password: newUser.password
    })
    .then(res => {
      return res.data.registrationSuccesful;
    });
};

export const login = user => {
  return axios
    .post("users/login", {
      email: user.email,
      password: user.password
    })
    .then(res => {
      if (res.data.token) {
        localStorage.setItem("usertoken", res.data.token);
        return true;
      } else {
        return false;
      }
    })
    .catch(err => {
      console.log(err);
    });
};

export const problemSolver = (data, vehicle_capacity) => {
  return axios
    .post("users/problemsolver", {
      data: data,
      vehicle_capacity: vehicle_capacity
    })
    .then(res => {
      return res.data;
    })
    .catch(err => {
      console.log(err);
    });
};
