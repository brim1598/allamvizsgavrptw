import React, { Component } from "react";

class Landing extends Component {
  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">
              Vehicle Routing Problem with Time Windows
            </h1>
          </div>
          <br />
          <br />
          <br />
          <blockquote className="blockquote">
            <p className="mb-0">
              The vehicle routing problem with time windows is concerned with
              the optimal routing of a ﬂeet of vehicles between a depot and a
              number of customers that must be visited within a speciﬁed time
              interval, called a time window. The purpose of this thesis is to
              develop new and efﬁcient solution techniques for solving the
              vehicle routing problem with time windows (VRPTW).
            </p>
          </blockquote>
        </div>
      </div>
    );
  }
}

export default Landing;
